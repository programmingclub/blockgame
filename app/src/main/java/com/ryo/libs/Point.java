package com.ryo.libs;

public class Point extends Vector{
	
	public Point(double par1X, double par2Y){
		super(par1X, par2Y);
	}
	
	public Point add(Point p){
		return new Point(x + p.x, y + p.y);
	}
	
	public Point sub(Point p){
		return new Point(x - p.x, y - p.y);
	}
}
