package com.ryo.libs;

/// <summary>
/// FPS制御用クラス。元々C#用(笑
/// </summary>
public class FPSControler
{
    // フレーム時刻の基準となる時刻（単位：ms）
    // System.Environment.TickCount により取得
    private long baseTickCount = 0;

    // 前回のフレームの時刻（単位：?s）
    // baseTickCountからの差分で表す
    private int prevTickCount = 0;

    // 現在のフレームの時刻（単位：μs）
    // baseTickCountからの差分で表す
    private int nowTickCount = 0;

    // 1フレームの時間（単位：μs）
    // デフォルト値＝60FPS≒1.66μs
    private int period = 1000 * 1000 / 60;

    // 目標のFPS
    // デフォルト値＝60FPS
    private int fps = 60;

    // 計測した（実際の）FPS
    private int fpsReal = 0;

    // FPS計測用のカウンタ値
    private int fpsCount = 0;

    // FPS測定用の時刻（単位：ms）
    private long fpsTickCount = 0;

    /// <summary>
    /// コンストラクタ
    /// </summary>
    /// <param name="fps">目標のFPS</param>
    public FPSControler(int fps)
    {
        baseTickCount = System.currentTimeMillis();
        this.fps = fps;
        period = 1000 * 1000 / fps;
    }

    /// <summary>
    /// 次のフレームを待つ。ゲームループの先頭で呼び出すことを期待している。
    /// </summary>
    /// <returns>常にtrueを返す（将来拡張用）</returns>
    public boolean WaitNextFrame(Thread thread)
    {
        prevTickCount += period;
        nowTickCount = (int)(System.currentTimeMillis() - baseTickCount) * 1000;

        if (nowTickCount >= (prevTickCount + period))
        {
            return true;
        }

        while (nowTickCount < (prevTickCount + period))
        {
        	try {
					thread.sleep(1);
				} catch (Exception e) {}
            nowTickCount = (int)(System.currentTimeMillis() - baseTickCount) * 1000;
        }

        return true;
    }

    /// <summary>
    /// このフレームの描画が行えるかどうかを返す。
    /// 現在の時刻と次のフレームの時刻の差から描画する余裕があれば ture，
    /// 余裕がなければ false を返す
    /// </summary>
    /// <returns>true:描画可能 false:描画不可</returns>
    public boolean IsDraw()
    {
        nowTickCount = (int)(System.currentTimeMillis() - baseTickCount) * 1000;

        if (nowTickCount < (prevTickCount + period * 2))
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// FPSを計算する。描画を実施したら呼ぶことを期待している。
    /// FPSを計測しない場合は呼ぶ必要はない。
    /// </summary>
    public void CalcFps()
    {
        fpsCount++;

        long tickCount = System.currentTimeMillis();
        if (tickCount - fpsTickCount >= 1000)
        {
            fpsReal = (int)((fpsCount * 1000) / (tickCount - fpsTickCount));
            fpsTickCount = tickCount;
            fpsCount = 0;
        }
    }

    /// <summary>
    /// FPS（目標値）を取得する
    /// </summary>
    /// <returns>FPS（目標値）</returns>
    public int GetFps()
    {
        return fps;
    }

    /// <summary>
    /// FPS（計測値）を取得する
    /// </summary>
    /// <returns>FPS（計測値）</returns>
    public int GetFpsReal()
    {
        return fpsReal;
    }
}

