package com.ryo.libs;

public class Helper {
	
    /// <summary>
    /// 線分同士の当たり判定を行う
    /// </summary>
    public static boolean GetIsHitLineAndLine(Line par1Line, Line par2Line)
    {
        Vector av = par1Line.endPos.sub(par1Line.startPos);
        Vector bv = par2Line.startPos.sub(par1Line.startPos);
        Vector cv = par2Line.endPos.sub(par1Line.startPos);
        Vector dv = par2Line.endPos.sub(par2Line.startPos);
        Vector ev = par1Line.startPos.sub(par2Line.startPos);
        Vector fv = par1Line.endPos.sub(par2Line.startPos);

        double a = Helper.GetCrossProduct(av, bv);
        double b = Helper.GetCrossProduct(av, cv);
        double c = Helper.GetCrossProduct(dv, ev);
        double d = Helper.GetCrossProduct(dv, fv);

        return (((a * b) < 0.0d) && ((c * d) < 0.0d));
    }

    /// <summary>
    /// 矩形と点の当たり判定を行う
    /// </summary>
    public static boolean GetIsHitRectAndPoint(Rect par1Rect, Point par2Point)
    {
        return (par1Rect.leftTopPos.x < par2Point.x) &&
               (par2Point.x < par1Rect.rightBottomPos.x) &&
               (par1Rect.leftTopPos.y < par2Point.y) &&
               (par2Point.y < par1Rect.rightBottomPos.y);
    }

    /// <summary>
    /// 矩形と線分の当たり判定を行う
    /// </summary>
    public static boolean GetIsHitLineAndRect(Line par1Line, Rect par2Rect)
    {
        if (GetIsHitRectAndPoint(par2Rect, par1Line.startPos) ||
            GetIsHitRectAndPoint(par2Rect, par1Line.endPos))
        {
            return true;
        }

        return GetIsHitLineAndLine(par1Line, new Line(par2Rect.leftTopPos, par2Rect.rightBottomPos)) ||
               GetIsHitLineAndLine(par1Line, new Line(new Point(par2Rect.rightBottomPos.x, par2Rect.leftTopPos.y),
                                                      new Point(par2Rect.leftTopPos.x, par2Rect.rightBottomPos.y)));
    }
    
    /// <summary>
    /// ベクトルの内積を求める
    /// </summary>
    public static double GetDotProduct(Vector par1Vector, Vector par2Vector)
    {
        return par1Vector.x * par2Vector.x + par1Vector.y * par2Vector.y;
    }

    /// <summary>
    /// ベクトルの外積を求める
    /// </summary>
    public static double GetCrossProduct(Vector par1Vector, Vector par2Vector)
    {
        return par1Vector.x * par2Vector.y - par1Vector.y * par2Vector.x;
    }
}
