package com.ryo.libs;

public class Line {
	public Point startPos;
	public Point endPos;
	
	public Line(Point par1StartPos, Point par2EndPos){
		startPos = par1StartPos;
		endPos = par2EndPos;
	}
}
