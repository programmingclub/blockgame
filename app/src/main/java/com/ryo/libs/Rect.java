package com.ryo.libs;

public class Rect {
	
    public Point leftTopPos;
    public Point rightBottomPos;

    public Rect(Point par1Point, Point par2Point)
    {
        leftTopPos = par1Point;
        rightBottomPos = par2Point;
    }

    public Rect(Point par1Point, double par2Width, double par3Height)
    {
        leftTopPos = par1Point;
        rightBottomPos = new Point(par1Point.x + par2Width, par1Point.y + par3Height);
    }
}
