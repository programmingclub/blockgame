package com.ryo.blockgame.entity.block;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.ryo.blockgame.R;
import com.ryo.blockgame.entity.EntityBall;
import com.ryo.blockgame.entity.EntityManager;
import com.ryo.blockgame.entity.EntityPlayer;

public class EntityBlockWithBall extends EntityBlock{

    private static Bitmap blockBitmap;

    public EntityBlockWithBall(double par1PosX, double par2PosY) {
        super(par1PosX, par2PosY);
    }

    public static void init(Context context){
        blockBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.block3);
    }

    @Override
    public void onDraw(int par1GameTickCount, Canvas par2Canvas) {
        par2Canvas.drawBitmap(blockBitmap, (float)posX, (float)posY, new Paint());
    }

    @Override
    public void destory(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager){
        par3EntityManager.spawnEntity(new EntityBall(posX, posY, par2EntityPlayer));
        isDeath = true;
    }
}
