package com.ryo.blockgame.entity;

import com.ryo.blockgame.GD;
import com.ryo.blockgame.R;
import com.ryo.blockgame.field.FieldGameMain;
import com.ryo.libs.Helper;
import com.ryo.libs.Point;
import com.ryo.libs.Rect;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.SoundPool;

public class EntityGameOvered extends EntityBase{

	private static Paint textPaint;
    private static Paint textPaint2;
	private static Rect buttonRect = new Rect(new Point(100, 450), new Point(1000, 700));
	private static SoundPool soundPool;
    private static Context mContext;
	private static int soundID;
	
	private int gameOverTime;
	
	public EntityGameOvered(double par1PosX, double par2PosY) {
		super(par1PosX, par2PosY);
		
		textPaint.setARGB(255, 255, 255, 255);
		textPaint.setAntiAlias(true);
		textPaint.setTextSize(90);
        textPaint.setTypeface(Typeface.create("", 2));

        textPaint2.setARGB(255, 255, 255, 255);
        textPaint2.setAntiAlias(true);
        textPaint2.setTextSize(90);
        textPaint2.setTypeface(Typeface.create("", 1));
	}
	
	public static void init(Context context){
		textPaint = new Paint();
        textPaint2 = new Paint();
		soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		soundID = soundPool.load(context, R.raw.ondeath, 1);
        mContext = context;
	}

	@Override
	protected void onUpdate(int par1GameTickCount,
			EntityPlayer par2EntityPlayer, EntityManager par3EntityManager,
			EntityManager par4BlockManager) {
		
		if ((GD.toughtAction == 0) &&
                Helper.GetIsHitRectAndPoint(buttonRect, new Point(GD.toughtPosX, GD.toughtPosY))){
			FieldGameMain.RestartGame();
		}
		
		if (gameOverTime == 0){
			playSound();
		}
		
		gameOverTime++;
	}

	@Override
	public void onDraw(int par1GameTickCount, Canvas par2Canvas) {
		if (255 < gameOverTime){
			par2Canvas.drawColor(Color.argb(255, 0, 0, 0));
		}
		else {
			par2Canvas.drawColor(Color.argb(gameOverTime, 0, 0, 0));
		}
		textPaint.setAlpha(gameOverTime % 150 + 106);
        par2Canvas.drawText("Game Over:(", 300, 400, textPaint2);
		par2Canvas.drawText("Touch to Retry", 255, 600, textPaint);
	}

    private static void playSound(){
        AudioManager audioManager = (AudioManager)mContext.getSystemService(mContext.AUDIO_SERVICE);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        int maxVolume  = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        float volumeRate = (float)currentVolume / maxVolume;

        soundPool.play(soundID, volumeRate, volumeRate, 1, 0, 1.0f);
    }
}
