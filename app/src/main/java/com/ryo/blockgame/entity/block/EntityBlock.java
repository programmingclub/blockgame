package com.ryo.blockgame.entity.block;

import com.ryo.blockgame.R;
import com.ryo.blockgame.entity.EntityBase;
import com.ryo.blockgame.entity.EntityManager;
import com.ryo.blockgame.entity.EntityPlayer;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class EntityBlock extends EntityBase {

	private static Bitmap blockBitmap;
	
	public EntityBlock(double par1PosX, double par2PosY){
		super(par1PosX, par2PosY);
	}
	
	public static void init(Context context) {
		blockBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.block);
	}

	@Override
	protected void onUpdate(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager) {}

	@Override
	public void onDraw(int par1GameTickCount, Canvas par2Canvas) {
		par2Canvas.drawBitmap(blockBitmap, (float)posX, (float)posY, new Paint());
	}

	public void destory(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager){
		isDeath = true;
	}
}
