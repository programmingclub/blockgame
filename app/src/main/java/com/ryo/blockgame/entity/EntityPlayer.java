package com.ryo.blockgame.entity;

import com.ryo.blockgame.GD;
import com.ryo.blockgame.R;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

public class EntityPlayer extends EntityBase{

	private static Bitmap playerBitmap;

    public int havingBallsNum;
	
	public EntityPlayer(double par1PosX, double par2PosY){
		super(par1PosX, par2PosY);
	}
	
	public static void init(Context context){
		playerBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.board);
	}
	
	@Override
	protected void onUpdate(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager) {
		if (GD.toughtPosX != Float.NaN){
			posX = GD.toughtPosX - 100;
		}
	}

	@Override
	public void onDraw(int par1GameTickCount, Canvas par2Canvas) {
		par2Canvas.drawBitmap(playerBitmap, (float)posX, (float)posY, new Paint());
	}

}
