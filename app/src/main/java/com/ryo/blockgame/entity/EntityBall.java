package com.ryo.blockgame.entity;

import com.ryo.blockgame.R;
import com.ryo.blockgame.entity.block.EntityBlock;
import com.ryo.libs.Helper;
import com.ryo.libs.Line;
import com.ryo.libs.Point;
import com.ryo.libs.Rect;

import android.R.id;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class EntityBall extends EntityBase {

	private static Bitmap ballBitmap;
	private static SoundPool soundPool;
    private static Context mContext;
	private static int ballSoundID;
	
	public EntityBall(double par1PosX, double par2PosY, EntityPlayer par3EntityPlayer){
		super(par1PosX, par2PosY);
        par3EntityPlayer.havingBallsNum++;
		vector.y = 5;
	}
	
	public static void init(Context context){
		ballBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ball);
		soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		ballSoundID = soundPool.load(context, R.raw.ball, 1);
        mContext = context;
	}
	
	@Override
	protected void onUpdate(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager) {
		if (Helper.GetIsHitLineAndLine(new Line(new Point(posX, posY), new Point(posX + vector.x, posY + vector.y)),
									   new Line(new Point(0, 0), new Point(1080, 0)))									||
			Helper.GetIsHitLineAndLine(new Line(new Point(posX + 16, posY), new Point(posX + vector.x + 16, posY + vector.y)),
									   new Line(new Point(0, 0), new Point(1080, 0)))){
			vector.y = Math.abs(vector.y * 1.1);
			playBallSound();
		}
		
		if (Helper.GetIsHitLineAndLine(new Line(new Point(posX, posY), new Point(posX + vector.x, posY + vector.y)),
											    new Line(new Point(0, 0), new Point(0, 1920)))									||
			Helper.GetIsHitLineAndLine(new Line(new Point(posX + 16, posY), new Point(posX + vector.x + 16, posY + vector.y)),
									   new Line(new Point(0, 0), new Point(0, 1920)))){
			vector.x = -(vector.x * 1.1);
			playBallSound();
		}
		
		if (Helper.GetIsHitLineAndLine(new Line(new Point(posX, posY), new Point(posX + vector.x, posY + vector.y)),
									   new Line(new Point(1080, 0), new Point(1080, 1920)))									||
			Helper.GetIsHitLineAndLine(new Line(new Point(posX + 16, posY), new Point(posX + vector.x + 16, posY + vector.y)),
									   new Line(new Point(1080, 0), new Point(1080, 1920)))){
			vector.x = -(vector.x * 1.1);
			playBallSound();
		}
		
		if (Helper.GetIsHitLineAndRect(new Line(new Point(posX, posY), new Point(posX + vector.x, posY + vector.y)),
									   new Rect(new Point(par2EntityPlayer.posX, par2EntityPlayer.posY), 200, 32))				||
			Helper.GetIsHitLineAndRect(new Line(new Point(posX + 16, posY), new Point(posX + vector.x + 16, posY + vector.y)),
									   new Rect(new Point(par2EntityPlayer.posX, par2EntityPlayer.posY - 32), 200, 32))){
			vector.x += (posX - (par2EntityPlayer.posX + 100)) / 50;
			vector.y = -(vector.y * 1.1);
			playBallSound();
		}
		
		for (EntityBase e : par4BlockManager.getEntities()) {
			if ((e != null) && !e.isDeath){
				if (doCollision(e)){
					((EntityBlock)e).destory(par1GameTickCount, par2EntityPlayer, par3EntityManager, par4BlockManager);
				}
			}
		}
		
		if (13 < vector.x) { vector.x = 13; }
		if (13 < vector.y) { vector.y = 13; }
		if (vector.x < -13) { vector.x = -13; }
		if (vector.y < -13) { vector.y = -13; }
		
		if (par2EntityPlayer.posY + 300 < posY){
            onOvered(par1GameTickCount, par2EntityPlayer, par3EntityManager, par4BlockManager);
		}
	}

	@Override
	public void onDraw(int par1GameTickCount, Canvas par2Canvas) {
		par2Canvas.drawBitmap(ballBitmap, (float)posX + ballBitmap.getWidth() / 2, (float)posY + ballBitmap.getHeight() / 2, new Paint());
	}

    protected void onOvered(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager) {
        par2EntityPlayer.havingBallsNum--;
        isDeath = true;
    }

	private boolean doCollision(EntityBase e){
        Rect blockRect = new Rect(new Point(e.posX, e.posY), 100, 32);

		if (Helper.GetIsHitRectAndPoint(new Rect(new Point(posX - 101, posY - (32 + 16)),
                    new Point(posX + 17, posY + 17)), new Point(e.posX, e.posY))          &&
                Helper.GetIsHitLineAndRect(new Line(new Point(posX, posY),
                        new Point(posX + vector.x, posY + vector.y)), blockRect)          ||
			    Helper.GetIsHitLineAndRect(new Line(new Point(posX + 16, posY), new Point(posX + vector.x + 16, posY + vector.y)), blockRect)){
			vector.x = (vector.x * 1.1);
			vector.y = -(vector.y * 1.1);
			playBallSound();
			
			return true;
		}
		
		return false;
	}
	
	private static void playBallSound(){
        AudioManager audioManager = (AudioManager)mContext.getSystemService(mContext.AUDIO_SERVICE);
        int currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
        int maxVolume  = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
        float volumeRate = (float)currentVolume / maxVolume;

        soundPool.play(ballSoundID, volumeRate, volumeRate, 1, 0, 1.0f);
	}
}
