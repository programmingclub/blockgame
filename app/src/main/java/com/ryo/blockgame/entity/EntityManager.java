package com.ryo.blockgame.entity;

import android.graphics.Canvas;

public class EntityManager {
	
	private int beforeUsedEntitiesArryIndex;
	private EntityBase[] entities;
	
	public EntityManager(int entitiesNum){
		entities = new EntityBase[entitiesNum];
	}
	
	public void updateEntities(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager){
		for (EntityBase e : entities) {
			if (e != null){
				if (e.isDeath){
					e = null;
				}
				else {
					e.update(par1GameTickCount, par2EntityPlayer, par3EntityManager, par4BlockManager);
				}
			}
		}
	}
	
	public void drawEntities(int par1GameTickCount, Canvas par2Canvas){
		for (EntityBase e : entities) {
			if (e != null){
				if (e.isDeath){
					e = null;
				}
				else {
					e.onDraw(par1GameTickCount, par2Canvas);
				}
			}
		}
	}
	
	public void spawnEntity(EntityBase entityBase){
		for (int i = beforeUsedEntitiesArryIndex; i < entities.length; i++) {
			if ((entities[i] == null) || (entities[i].isDeath)){
				entities[i] = entityBase;
				//以前使ったentitiesの配列を記録することで、
				//空きスペースの検索を高速化
				beforeUsedEntitiesArryIndex = i;
				return;
			}
		}
		
		for (int i = 0; i < entities.length; i++) {
			if ((entities[i] == null) || (entities[i].isDeath)){
				entities[i] = entityBase;
				beforeUsedEntitiesArryIndex = i;
				return;
			}
		}
	}
	
	public EntityBase[] getEntities(){
		return entities;
	}
}
