package com.ryo.blockgame.entity;

import android.content.res.Resources;
import android.graphics.Canvas;

import com.ryo.libs.Vector;

public abstract class EntityBase {
	
	public double posX;
	public double posY;
	public boolean isDeath;
	public Vector vector = new Vector(0, 0);
	
	public EntityBase(double par1PosX, double par2PosY){
		posX = par1PosX;
		posY = par2PosY;
	}
	
	public void update(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager){
		onUpdate(par1GameTickCount, par2EntityPlayer, par3EntityManager, par4BlockManager);
		posX += vector.x;
		posY += vector.y;
	}
	
	protected abstract void onUpdate(int par1GameTickCount, EntityPlayer par2EntityPlayer, EntityManager par3EntityManager, EntityManager par4BlockManager);
	
	public abstract void onDraw(int par1GameTickCount, Canvas par2Canvas);
}
