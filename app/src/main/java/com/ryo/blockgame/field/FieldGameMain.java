package com.ryo.blockgame.field;

import java.util.Random;

import com.ryo.blockgame.GD;
import com.ryo.blockgame.R;
import com.ryo.blockgame.entity.EntityBall;
import com.ryo.blockgame.entity.EntityGameOvered;
import com.ryo.blockgame.entity.EntityManager;
import com.ryo.blockgame.entity.EntityPlayer;
import com.ryo.blockgame.entity.block.EntityBlock;
import com.ryo.blockgame.entity.block.EntityBlockWithBall;
import com.ryo.blockgame.entity.block.EntityHeardBlock;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class FieldGameMain extends FieldBase{

	private static boolean isRequestedRestart;
	private EntityManager entityManager;
	private EntityManager blockManager;
	private EntityPlayer player;
	
	public FieldGameMain(){
		initGame();
	}
	
	public static void init(Context context) {
		EntityBlock.init(context);
		EntityHeardBlock.init(context);
		EntityPlayer.init(context);
		EntityBall.init(context);
		EntityGameOvered.init(context);
        EntityBlockWithBall.init(context);
	}
	
	public static void RestartGame(){
		isRequestedRestart = true;
	}

	@Override
	public void update(int par1GameTickCount) {
		if (isRequestedRestart){
			initGame();
		}
		
		if (!player.isDeath &&
				player.havingBallsNum == 0){
			entityManager.spawnEntity(new EntityGameOvered(0, 0));
			player.isDeath = true;
		}
		else {
			player.update(par1GameTickCount, null, entityManager, blockManager);
		}
		
		blockManager.updateEntities(par1GameTickCount, player, entityManager, blockManager);
		entityManager.updateEntities(par1GameTickCount, player, entityManager, blockManager);
	}

	@Override
	public void draw(int par1GameTickCount, Canvas par2Canvas) {
		
		player.onDraw(par1GameTickCount, par2Canvas);
		
		blockManager.drawEntities(par1GameTickCount, par2Canvas);
		entityManager.drawEntities(par1GameTickCount, par2Canvas);
	}
	
	private void initGame(){
		isRequestedRestart = false;
		player = new EntityPlayer(450, 1500);
		entityManager = new EntityManager(16);
		blockManager = new EntityManager(100);
		
		Random random = new Random();
		for (int y = 0; y < 7; ++y){
			for (int x = 0; x < 11; ++x){
                if (random.nextInt(20) == 0){
                    blockManager.spawnEntity(new EntityBlockWithBall(x * 100, y * 32 + 32 * 3));
                }
				else if (random.nextInt(3) == 0){
					blockManager.spawnEntity(new EntityHeardBlock(x * 100, y * 32 + 32 * 3));
				}
				else {
					blockManager.spawnEntity(new EntityBlock(x * 100, y * 32 + 32 * 3));
				}
			}
		}
		
		entityManager.spawnEntity(new EntityBall(540, 500, player));
		GD.backGroundColor = Color.rgb(220, 235, 220);
	}

}
