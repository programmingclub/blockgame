package com.ryo.blockgame.field;

import android.content.res.Resources;
import android.graphics.Canvas;

public abstract class FieldBase {

    public static void init(Resources res){}

    public abstract void update(int par1GameTickCount);

    public abstract void draw(int par1GameTickCount, Canvas par2Canvas);
}
