package com.ryo.blockgame;

import com.ryo.blockgame.field.FieldGameMain;
import com.ryo.libs.FPSControler;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameMain extends SurfaceView implements SurfaceHolder.Callback,Runnable{
	
	private FPSControler fpsControler;
	private SurfaceHolder holder;
	private Thread thread;
	private FieldGameMain fieldGame;
	private int gameTickCount;
	
	public GameMain(Context context) {
		super(context);
		
		//initialize
		fpsControler = new FPSControler(60);
		holder = getHolder();
		holder.addCallback(this);
		GD.screenWidth = getWidth();
		GD.screenHeight = getHeight();
		holder.setFixedSize(GD.screenWidth, GD.screenHeight);
		System.out.println("W:" + GD.screenWidth + " H:" + GD.screenHeight);
		FieldGameMain.init(context);
		fieldGame = new FieldGameMain();
	}

	@Override
	public void run() {
		Canvas canvas;
		while ((thread != null) && (fpsControler.WaitNextFrame(thread))){
			gameTickCount++;
			//ココにUpdate処理
			fieldGame.update(gameTickCount);
			
			if (fpsControler.IsDraw()){
				canvas = holder.lockCanvas();
				if (canvas == null) { break; }
				canvas.drawColor(GD.backGroundColor);
				
				//ココに描画処理
				fieldGame.draw(gameTickCount, canvas);
				
				holder.unlockCanvasAndPost(canvas);
			}
		}
	}

	public boolean onTouchEvent(MotionEvent event){
		GD.toughtPosX = event.getX();
		GD.toughtPosY = event.getY();
        GD.toughtAction = event.getAction();
		
		return true;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) { }

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		thread = null;
	}
	
}
